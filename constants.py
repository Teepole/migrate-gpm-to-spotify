GPM_CACHE = "./.cache-gpm"
SPOTIFY_CACHE = "./.cache-spotify"
LOG = "./log.txt"
FOLLOW_SCOPE = "user-follow-modify"

## Retrieve these from the spotify dev dashboard
CLIENT_ID = ""
CLIENT_SECRET = ""
REDIRECT_URI = ""
CLIENT_USER = ""
